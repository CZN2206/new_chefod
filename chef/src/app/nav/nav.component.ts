import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from '../services/auth.service';
import { AuthGroup } from '../models/AuthGroup';

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class NavComponent implements OnInit {
  authgroup: AuthGroup = 1;
  constructor(private router: Router, public auth: AuthService) {}

  ngOnInit() {
    if (window.localStorage) {
      if (!localStorage.getItem("firstLoad")) {
        console.log('APP nav init')
        localStorage["firstLoad"] = true;
        window.location.reload();
      } else localStorage.removeItem("firstLoad");
    }
  }

  logout() {
    console.log('clear session')
    sessionStorage.clear();
    this.router.navigate(["/login"]);
  }
}
