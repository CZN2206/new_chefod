import { Injectable, ɵConsole } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AuthGroup } from '../models/AuthGroup';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(protected router: Router,
    protected authorizationService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): Promise<boolean> | boolean {
    return this.hasRequiredPermission(route.data['auth']);
  }

  protected hasRequiredPermission(authGroup: AuthGroup[]): Promise<boolean> | boolean {
    // If user’s permissions already retrieved from the API
    if (this.authorizationService.permissions) {
      console.log('check if alreadty has guards');
      console.log(authGroup);
      if (authGroup) {
        console.log('chefck for one role');
        return this.checkForAtleastOneRole(authGroup);
      } else {
        return this.authorizationService.hasPermission(null);
      }
    }
    else{
      
        this.authorizationService.initializePermissions();
        return this.checkForAtleastOneRole(authGroup);
    }
  }

  checkForAtleastOneRole(authGroup: AuthGroup[]) : boolean{
    var onePermissionFound: boolean = false;
    console.log(authGroup.length);
    for(var i:number=0; i< authGroup.length; i++){
      console.log('inside looop')
      console.log(authGroup[i])
      if(this.authorizationService.hasPermission(authGroup[i])){
        onePermissionFound = true;
      }
    }
    console.log('ONE PERM FOUND '+ onePermissionFound);
    if(onePermissionFound){
     return true;
    }
    else{
      return false;
    }
  }
  
}
