import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from '../models/admin';
import { AdminService } from '../services/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';
import { AuthDataService } from '../services/auth-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isBusy: boolean;
  notBusy: boolean;
  showErrorMessage: boolean;
  constructor(private router: Router, private loginService: AdminService, private formBuilder: FormBuilder, 
    private _authDataService: AuthDataService, private _authenticationService: AuthService, ) { }

  ngOnInit() {   
    sessionStorage.clear(); 
    this.loginForm = this.formBuilder.group(
      {username: ['', Validators.email],
      password:['',Validators.required
    ]}
    );
    this.isBusy = false;
    this.notBusy = true;
  }

  get formData(){
    return this.loginForm.controls;
}

private setUserPermissions(userType: number): void {
  console.log('set permissions!!!!!!');
  this._authDataService.setUserPermissions(userType);
  this._authenticationService.initializePermissions();
}

  login(){
    this.isBusy = true;
    this.notBusy = false;
    if(this.loginForm.invalid){
      Swal.fire(
        {
          title: 'Warning',
          text: 'Please enter valid email and password!',
          type: 'warning',
          confirmButtonColor: '#AA122B',
         }

      )
     return;
    }    

    //this.auth.sendToken(this.formData.username.value)
     // this.router.navigate(["home"]);

  this.loginService.login(this.formData.username.value, this.formData.password.value).pipe(first())
  .subscribe(
    data => {
      sessionStorage.setItem('User', JSON.stringify(data));
      sessionStorage.setItem('UserType', data.role);
      sessionStorage.setItem('Username', this.formData.username.value);
      this.isBusy = false;
      this.notBusy = true;
      debugger;
      this.setUserPermissions(data.role);

      if(data.role === '1'){
        this.router.navigate(['/admin-home']);
      }
      if(data.role === '3'){
        this.router.navigate(['/chef-home']);
      }
                
    },
    error => {
      this.isBusy = false;
      this.notBusy = true;
      console.log(this.isBusy);
      console.log(this.notBusy);
      console.log(error.error.message);
      Swal.fire(
        {
          title: 'Error',
          text: error.error.message,
          type: 'error',
          confirmButtonColor: '#AA122B',
         }

      )
    }
  )
  }

}
