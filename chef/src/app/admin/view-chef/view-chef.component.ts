import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ChefDataService } from "src/app/services/chef-data.service";
import { AdminService } from "src/app/services/admin.service";
import { first } from "rxjs/operators";
import { Chef } from "src/app/models/chef";
import Swal from "sweetalert2";
import { DomSanitizer } from '@angular/platform-browser';
//import { swalDefaultsProvider } from '@sweetalert2/ngx-sweetalert2/di';

@Component({
  selector: "app-view-chef",
  templateUrl: "./view-chef.component.html",
  styleUrls: ["./view-chef.component.css"]
})
export class ViewChefComponent implements OnInit {
  chefId: string;
  chefDetails: any;
  InternalId: any;
  url: any;
  isBusy: boolean;
  isActive: boolean;

  idAvailable: boolean;
  pcAvailable: boolean;
  qualAvailable: boolean;
  refAvailable: boolean;

  constructor(
    private router: Router,
    private chefData: ChefDataService,
    private adminService: AdminService,
    private activated: ActivatedRoute,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    //window.scrollTo(0, 0);
    this.isActive = false;
    this.qualAvailable = false;
    this.pcAvailable = false;
    this.refAvailable = false;
    this.idAvailable = false;
    console.log(sessionStorage.getItem('User'));
    this.activated.params.subscribe(params => {
      this.InternalId = params.internalID;
    });
    this.getChefDetails();
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(
      `linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`
    );
  }

  getChefDetails() {
    this.adminService
      .getChefDetails(this.InternalId)
      .pipe(first())
      .subscribe(
        data => {
          this.chefDetails = data;
          if(this.chefDetails.chefDetails.status == 'Active')
          {
            this.isActive = true;
          }
          else{
            this.isActive = false;
          }

          if(this.chefDetails.chefDetails.idDocument != ""){
            this.idAvailable = true;
          }
          if(this.chefDetails.chefDetails.policeClearance != ""){
            this.pcAvailable = true;
          }
          if(this.chefDetails.chefDetails.qualifications != ""){
            this.qualAvailable = true;
          }
          if(this.chefDetails.chefDetails.referrals != ""){
            this.refAvailable = true;
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  readUrl(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
      }
  
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  contactChef() {
    this.router.navigate(["/contact-chefs"]);
  }

  updateAddress() {
    this.router.navigate(["/address-update-admin/"+ this.InternalId]);
  }

  updatePersonal() {
    console.log('clicked personal');
    this.router.navigate(["/personal-update-admin/"+ this.InternalId]);
  }

  updateBanking() {
    this.router.navigate(["/banking-update-admin/"+ this.InternalId]);
  }

  updateDocs() {
    this.router.navigate(["/documents-update-admin/"+ this.InternalId]);
  }

  updateProfession() {
    this.router.navigate(["/profession-update-admin/"+ this.InternalId]);
  } 

  downloadPDF(name: string){
    var linkSource;
    if(name == 'idDocument'){
       linkSource = this.chefDetails.chefDetails.idDocument;
    }
    if(name == 'referral'){
       linkSource = this.chefDetails.chefDetails.referrals;
    }
    if(name == 'qualification'){
       linkSource = this.chefDetails.chefDetails.qualifications;
    }
    if(name == 'police'){
       linkSource = this.chefDetails.chefDetails.policeClearance;
    }
  
    const downloadLink = document.createElement("a");
    const fileName = name+'.pdf';
  
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
 
  approveChef(status: string){
    this.isBusy = true;
    this.adminService.approveChef(this.InternalId,status).pipe(first()).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Chef status successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/manage-chefs"]));
      },
      error => {
        this.isBusy = false;
        Swal.fire({
          title: "Error",
          text: error.error.error,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );
  }

}
