import { Component, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ChefInvite } from "src/app/models/chefInvite";
import { AdminService } from "src/app/services/admin.service";
import { first } from "rxjs/operators";
import { AreaOfExpertise } from "src/app/models/areaOfExpertise";
import { OperationalRegion } from "src/app/models/operationalRegion";
import { CuisineType } from "src/app/models/cuisineType";
import Swal from "sweetalert2";

@Component({
  selector: "app-manage-chefs",
  templateUrl: "./manage-chefs.component.html",
  styleUrls: ["./manage-chefs.component.css"]
})
export class ManageChefsComponent implements OnInit {

  private invalidEmailSwal: SwalComponent;
  addChefForm: FormGroup;
  selectedFile = null;
  isBusy: boolean;
  image: string = "";
  errorMessage: string;

  constructor(
    private _sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private router: Router,
    private adminService: AdminService
  ) {}

  ngOnInit() {
    this.isBusy = false;
    window.scrollTo(0, 0);
    this.addChefForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: ["", Validators.email]
    });

    
  }


 
  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(
      `linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`
    );
  }

  get formData() {
    return this.addChefForm.controls;
  }

  goHome() {
    this.router.navigate(["/admin-home"]);
  }

  createChef() {
    console.log("create cheffffffff");
    this.isBusy = true;
    if (this.addChefForm.invalid) {
      if (this.formData.email.valid) {
        this.isBusy = false;
        Swal.fire({
          title: "Warning",
          text: "Please fill in all fields!",
          type: "warning",
          confirmButtonColor: "#AA122B"
        });
      } else {
        this.isBusy = false;
        Swal.fire({
          title: "Warning",
          text: "Please enter a valid email!",
          type: "warning",
          confirmButtonColor: "#AA122B"
        });
      }
      return;
    }

    var chef: ChefInvite = new ChefInvite(
      this.formData.firstName.value,
      this.formData.lastName.value,
      this.formData.email.value
    );
    console.log(chef);

    this.adminService.inviteChef(chef)
      .pipe(first())
      .subscribe(
        data => {
          this.isBusy = false;
          Swal.fire({
            title: "All done",
            text: "Chef successfuly invited!",
            type: "success",
            confirmButtonColor: "#AA122B"
          }).then(result => this.router.navigate(["/admin-home"]));
        },
        error => {
          this.errorMessage = error.error.error;
          this.isBusy = false;
          Swal.fire({
            title: "Error",
            text: error.error.message,
            type: "error",
            confirmButtonColor: "#AA122B"
          });
        }
      );
  }

  
}
