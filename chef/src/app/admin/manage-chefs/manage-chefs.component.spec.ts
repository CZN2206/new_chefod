import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageChefsComponent } from './manage-chefs.component';

describe('ManageChefsComponent', () => {
  let component: ManageChefsComponent;
  let fixture: ComponentFixture<ManageChefsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageChefsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageChefsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
