import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactChefsComponent } from './contact-chefs.component';

describe('ContactChefsComponent', () => {
  let component: ContactChefsComponent;
  let fixture: ComponentFixture<ContactChefsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactChefsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactChefsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
