import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AdminService } from "src/app/services/admin.service";
import { Route } from "@angular/compiler/src/core";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { first } from "rxjs/operators";
import { ChefDataService } from "src/app/services/chef-data.service";

@Component({
  selector: "app-contact-chefs",
  templateUrl: "./contact-chefs.component.html",
  styleUrls: ["./contact-chefs.component.css"]
})
export class ContactChefsComponent implements OnInit {
  contactChefForm: FormGroup;
  contactMethod: string;
  sendmail: boolean;
  sendtext: boolean;
  internalId: string;
  smsSent: boolean;
  emailSent: boolean;
  notBusy: boolean;
  optionSelected: boolean;
  constructor(
    private adminService: AdminService,
    private router: Router,
    private formBuilder: FormBuilder,
    private chefData: ChefDataService
  ) {}

  ngOnInit() {
    this.notBusy = true;
    this.smsSent = false;
    this.emailSent = false;
    this.sendtext = false;
    this.sendmail = false;
    this.optionSelected = false;

    window.scrollTo(0, 0);

    this.contactChefForm = this.formBuilder.group({
      subject: [""],
      message: ["", Validators.required]
    });

    this.chefData.getChef().subscribe(chef => {
      this.internalId = chef;
    });
  }

  get formData() {
    return this.contactChefForm.controls;
  }

  changeMethod(contactMethod: string) {
    console.log(contactMethod);

    this.contactMethod = contactMethod;
    this.optionSelected = true;
    if (contactMethod === "Email") {
      this.sendmail = true;
    }
    if (contactMethod === "SMS") {
      this.sendtext = true;
    }

    console.log("this.sendmail " + this.sendmail);
    console.log("this.sendtext " + this.sendtext);
  }

  sendSms() {
    this.adminService
      .smsChef(
        sessionStorage.getItem("ChefContact"),
        "Chefsonapp",
        this.formData.message.value
      )
      .pipe(first())
      .subscribe(
        data => {
          // this.notBusy = true;
          console.log("sms sent!");
          this.smsSent = true;
          console.log(this.smsSent);
        },
        error => {
          // this.notBusy = true;
          console.log("sms not sent!");
          this.smsSent = false;
        }
      );
    //return this.smsSent;
  }

  sendEmail() {
    this.adminService
      .emailChef(
        this.internalId,
        this.formData.subject.value,
        this.formData.message.value
      )
      .pipe(first())
      .subscribe(
        data => {
          //this.notBusy = true;
          console.log("email sent!");

          this.emailSent = true;
        },
        error => {
          //this.notBusy = true;
          console.log("email not sent!");
          this.emailSent = false;
        }
      );
    // return this.emailSent;
  }

  sendMessage() {
    if (this.contactChefForm.invalid) {
      Swal.fire({
        title: "Warning",
        text: "Please ensure that a message is captured!",
        type: "warning",
        confirmButtonColor: "#AA122B"
      });
      return;
    }

    this.notBusy = false;

    if (this.sendEmail) {
      this.sendEmail();
    }

    if (this.sendSms) {
      this.sendSms();
    }

    console.log(this.smsSent);
    console.log(this.emailSent);
    this.notBusy = true;
    Swal.fire({
      title: "All done",
      text: "Message sent!",
      type: "success",
      confirmButtonColor: "#AA122B"
    }).then(result => this.router.navigate(["/manage-chefs"]));

    /*if(this.sendEmail && this.sendtext){
      console.log('send email and sms');
      if(this.sendEmail() || this.sendSms()){
        Swal.fire(
          {
            title: 'All done',
            text: 'Message sent!',
            type: 'success',
            confirmButtonColor: '#AA122B',
           }             
        ).then(
          result => this.router.navigate(['/manage-chefs'])  
        );        
      }
      this.emailSent =  this.sendEmail();
      this.smsSent = this.sendSms();
    }
    if(this.sendEmail && !this.sendtext){
      console.log('email and not sms');
      this.sendEmail();
    } 
    if(!this.sendEmail && this.sendtext){
      console.log('not email but text');
      this.sendSms();
    } 
    
    console.log('^^^^^^^^^^^^^^^^^^');
    console.log(this.smsSent);
    console.log(this.emailSent);


    if(this.smsSent || this.emailSent){
      this.notBusy = true;
      Swal.fire(
        {
          title: 'All done',
          text: 'Message sent!',
          type: 'success',
          confirmButtonColor: '#AA122B',
         }             
      ).then(
        result => this.router.navigate(['/manage-chefs'])  
      );
    }
    else{
      this.notBusy = true;
      Swal.fire(
        {
          title: 'Error',
          text: 'Message sending failed!',
          type: 'error',
          confirmButtonColor: '#AA122B',
         }             
      )   
    }*/
  }
}
