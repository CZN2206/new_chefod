import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { first } from "rxjs/operators";
import { AdminDishes } from "src/app/models/adminDishes";
import { AdminService } from "src/app/services/admin.service";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { Dish } from "src/app/models/dish";
import { DishDataService } from "src/app/services/dish-data.service";

@Component({
  selector: "app-approve-dishes",
  templateUrl: "./approve-dishes.component.html",
  styleUrls: ["./approve-dishes.component.css"]
})
export class ApproveDishesComponent implements OnInit {
  @ViewChild("fileInput", { static: false }) fileInput: ElementRef;
  allDishes: AdminDishes[] = [];
  errorMessage: string;
  dishImage: string;
  dishName: string;
  internalId: string;
  rejectBusy: boolean;
  approveBusy: boolean;
  rejectNotBusy: boolean;
  approveNotBusy: boolean;
  modalActive: boolean;
  status: string;
  buttonActive: boolean;
  loading: boolean;
  constructor(
    private adminService: AdminService,
    public router: Router,
    private dishData: DishDataService
  ) {}

  ngOnInit() {
    this.modalActive = false;
    this.buttonActive = false;
    this.loading = false;
    window.scrollTo(0, 0);
    this.getAllDishes();
    this.rejectBusy = false;
    this.rejectNotBusy = true;
    this.approveBusy = false;
    this.approveNotBusy = true;
  }

  getAllDishes() {
    this.loading = true;
    this.adminService
      .getDishes()
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.allDishes = data;
        },
        error => {
          console.log(error);
          this.loading = false;
          this.errorMessage = error.error.errorMessage;
          this.allDishes = [];
        }
      );
  }

  dishInfo(dish: AdminDishes) {
    this.dishImage = dish.picture;
    this.dishName = dish.itemName;
    this.internalId = dish.internalId;
    this.modalActive = true;
  }

  actionDish() {
    console.log(this.status);
    if (this.status === "Approved") {
      this.approveBusy = true;
      this.approveNotBusy = false;
    } else {
      this.rejectBusy = true;
      this.rejectNotBusy = false;
    }
    console.log(this.rejectNotBusy);
    console.log(this.approveNotBusy);

    this.adminService
      .approveDish(this.internalId, this.status)
      .pipe(first())
      .subscribe(
        data => {
          this.rejectBusy = false;
          this.rejectNotBusy = true;
          this.approveBusy = false;
          this.approveNotBusy = true;
          Swal.fire({
            title: "All done",
            text: "Dish status set to " + this.status,
            type: "success",
            confirmButtonColor: "#AA122B"
          }).then(result => this.modalActive = false);
        
          
        },
        error => {
          this.rejectBusy = false;
          this.rejectNotBusy = true;
          this.approveBusy = false;
          this.approveNotBusy = true;
          Swal.fire({
            title: "Error",
            text: "Faled to action dish!",
            type: "error",
            confirmButtonColor: "#AA122B"
          });
        }
      );

   // window.location.reload();
    this.getAllDishes();
  }

  changeStatus(status: string) {
    console.log(status);
    this.status = status;
    this.buttonActive = true;
  }
}
