import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveDishesComponent } from './approve-dishes.component';

describe('ApproveDishesComponent', () => {
  let component: ApproveDishesComponent;
  let fixture: ComponentFixture<ApproveDishesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveDishesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveDishesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
