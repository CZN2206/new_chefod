import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { DomSanitizer } from '@angular/platform-browser';
import { first } from 'rxjs/operators';
import { PersonalUpdate } from 'src/app/models/personalUpdate';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponentAdmin implements OnInit {
  personalUpdateForm: FormGroup;
  selectedPantsSize: string = "Pants size";
  selectedShirtSize: string = "Shirt size";
  isBusy: boolean;
  image: string = "";
  selectedFile = null;
  firstName: string = "";
  lastName: string = "";
  email: string = "";
  cellNumber: string = "";
  aboutChef: string = "";
  idNumber: string = "";
  sacaNumber: string = "";
  shirtSize: string = "";
  pantsSize: string = "";
  internalID: string = "";
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private adminService: AdminService,
    private _sanitizer: DomSanitizer,
    private activated: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isBusy = false;
    this.isBusy = false;
    this.loading = false;
    this.activated.params.subscribe(params => {
      this.internalID = params.internalID;
    });
    this.getChefDetails();

    this.personalUpdateForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: [{value: "", disabled: true},, Validators.required],
      cellNumber: ["", Validators.required],
      aboutChef: ["", Validators.required],
      idNumber: ["", Validators.required],
      sacaNumber: [""]
    });
  
  }

  getChefDetails() {
    this.loading = true;
    this.adminService
      .getChefDetails(this.internalID)
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.firstName = data.chefDetails.name;
          this.lastName = data.chefDetails.surname;
          this.email = data.chefDetails.email;
          this.cellNumber = data.chefDetails.contactNumber;
          this.sacaNumber = data.chefDetails.sacaNumber;
          this.idNumber = data.chefDetails.idNumber;
          this.aboutChef = data.chefDetails.aboutChef;
          this.selectedPantsSize = data.chefDetails.pantsSize;
          this.selectedShirtSize = data.chefDetails.shirtSize;
          this.image = data.chefDetails.avatar;
          this.internalID = data.chefDetails.id  ;
        },
        error => {
          console.log(error);
        }
      );
  }

  get formData() {
    return this.personalUpdateForm.controls;
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(
      `linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`
    );
  }

  onFileChanged($event): void {
    console.log("avatar selected!!!");
    console.log($event);
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    console.log(inputValue);
    this.selectedFile = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = e => {
      this.image = myReader.result as string;
    };

    console.log(this.image);
    myReader.readAsDataURL(this.selectedFile);
  }

  changePantsSize(pants: string){
    this.selectedPantsSize = pants;
  }

  changeShirtSize(shirt: string){
    this.selectedShirtSize = shirt;
  }

  updatePersonal() {
    var personalDetails: PersonalUpdate = new PersonalUpdate(
      this.internalID,
      this.image,
      this.formData.firstName.value,
      this.formData.lastName.value,
      this.formData.email.value,
      this.formData.cellNumber.value,
      this.formData.idNumber.value,
      this.formData.aboutChef.value,
      this.formData.sacaNumber.value,
      this.selectedPantsSize,
      this.selectedShirtSize
    )

    console.log(personalDetails);

    this.adminService.updatePersonal(personalDetails).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Personal Details successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/view-chef/"+ this.internalID]));
      },
      error => {
        this.isBusy = false;
        console.log(error.error.message);
        Swal.fire({
          title: "Error",
          text: error.error.message,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );

  }

}
