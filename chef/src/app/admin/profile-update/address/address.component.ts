import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { first } from 'rxjs/operators';
import { Address } from 'src/app/models/address';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponentAdmin implements OnInit {
  addressUpdateForm: FormGroup;
  line1: string = "";
  line2: string = "";
  city: string = "";
  province: string = "";
  postalCode: string = "";
  currentAddress: Address;
  isBusy: boolean;
  InternalId: string;
  loading: boolean;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService, private activated: ActivatedRoute) { }


  ngOnInit() {
    this.isBusy = false;
    this.loading = false;
    this.activated.params.subscribe(params => {
      this.InternalId = params.internalID;
    });

    this.getChefProfile();
    this.addressUpdateForm = this.formBuilder.group({
      line1: ["", Validators.required],
      line2: ["", Validators.required],
      city: ["", Validators.required],
      province: ["", Validators.required],
      postalCode: ["", Validators.required]
    });
  }

  getChefProfile() {
    console.log("go get entire chef profile");
    this.loading = true;
    this.adminService
      .getChefDetails(this.InternalId)
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.line1 = data.chefDetails.addressLine1;
          this.line2 = data.chefDetails.addressLine2;
          this.city = data.chefDetails.city;
          this.province = data.chefDetails.province;
          this.postalCode = data.chefDetails.postalCode;
          this.InternalId = data.chefDetails.id;
        },
        error => {
          console.log(error);
        }
      );
  }

  get formData() {
    return this.addressUpdateForm.controls;
  }

  updateAddress() {
    this.isBusy = true;
    var newAddress: Address = new Address(
      this.formData.line1.value,
      this.formData.line2.value,
      this.formData.city.value,
      this.formData.province.value,
      this.formData.postalCode.value,
      this.InternalId
    );

    console.log(newAddress);

    this.adminService.updateAddress(newAddress).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Address successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/view-chef/"+ this.InternalId]));
      },
      error => {
        this.isBusy = false;
        Swal.fire({
          title: "Error",
          text: error.error.error,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );
  }

}
