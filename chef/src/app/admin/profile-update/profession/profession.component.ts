import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ProfessionUpdate } from 'src/app/models/professionUpdate';
import { first } from 'rxjs/operators';
import { OperationalRegion } from 'src/app/models/operationalRegion';
import { CuisineType } from 'src/app/models/cuisineType';
import { AreaOfExpertise } from 'src/app/models/areaOfExpertise';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-profession',
  templateUrl: './profession.component.html',
  styleUrls: ['./profession.component.css']
})
export class ProfessionComponentAdmin implements OnInit {

  specialtyList = [];
  selectedSpecialties: string [] = [];
  cuisineList = [];
  selectedCuisines: string [] = [];
  areaList = [];
  selectedAreas: string [] = [];
  finalListOfSpecialties: string[] = [];
  finalListOfAreas: string[] = [];
  finalListOfCousines: string[] = [];
  dropdownSettings = {};  
  professionUpdateForm: FormGroup;
  isBusy: boolean;
  internalID: string;

  dropdownList : string [] = [];
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminService,private activated: ActivatedRoute) { }

  ngOnInit() {

    this.isBusy = false;
    this.isBusy = false;
    this.activated.params.subscribe(params => {
      this.internalID = params.internalID;
    });
    this.getChefDetails();

    this.specialtyList = [
      { item_id: 1, item_text: 'Hot Kitchen' },
      { item_id: 2, item_text: 'Baking' },
      { item_id: 3, item_text: 'Cold Kitchen' },
      { item_id: 4, item_text: 'All rounder' },
      { item_id: 5, item_text: 'Pastry' },
      { item_id: 6, item_text: 'Saucer' }
    ];

    this.cuisineList = [
      { item_id: 1, item_text: 'Italian' },
      { item_id: 2, item_text: 'Spanish' },
      { item_id: 3, item_text: 'French' },
      { item_id: 4, item_text: 'Greek' },
      { item_id: 5, item_text: 'Indian' },
      { item_id: 6, item_text: 'Chinese' },
      { item_id: 7, item_text: 'BBQ/Braai' },
      { item_id: 8, item_text: 'Mexican' },
      { item_id: 8, item_text: 'South African Cuisine' }
    ];

    this.areaList = [
      { item_id: 1, item_text: "Pretoria" },
      { item_id: 2, item_text: "Johannesburg" },
      { item_id: 3, item_text: "Cape Town" },
      { item_id: 4, item_text: "Sandton" },
      { item_id: 5, item_text: "Durban" },
      { item_id: 6, item_text: "Port Elizabeth" }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
 
  }

  onSpecialtySelect(item: any) {
    var areaOfExpertise: AreaOfExpertise = new AreaOfExpertise(item.item_text);

    item.item_text;
    this.finalListOfSpecialties.push(item.item_text);
  }

  onSelectAllSpecialties(items: any) {
    for (var i = 0; i < items.length; i++) {
      var areaOfExpertise: AreaOfExpertise = new AreaOfExpertise(
        items[i].item_text
      );

      this.finalListOfSpecialties.push(items[i].item_text);
    }
  }

  OnSpecialtyDeSelect(item: any) {
    this.finalListOfSpecialties.splice(
      this.finalListOfSpecialties.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllSpecialties(items: any) {
    this.finalListOfSpecialties = [];
  }

  onCuisineSelect(item: any) {
    this.finalListOfCousines.push(item.item_text);
  }
  onSelectAllCuisines(items: any) {
    for (var i = 0; i < items.length; i++) {
      var cuisine: CuisineType = new CuisineType(items[i].item_text);
      this.finalListOfCousines.push(items[i].item_text);
    }
  }

  OnCuisineDeSelect(item: any) {
    this.finalListOfCousines.splice(
      this.finalListOfCousines.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllCuisines(items: any) {
    this.finalListOfCousines = [];
  }

  onAreaSelect(item: any) {
    this.finalListOfAreas.push(item.item_text);
  }

  onSelectAllAreas(items: any) {
    for (var i = 0; i < items.length; i++) {
      var area: OperationalRegion = new OperationalRegion(items[i].item_text);
      this.finalListOfAreas.push(items[i].item_text);
    }
  }

  OnAreasDeSelect(item: any) {
    this.finalListOfAreas.splice(
      this.finalListOfAreas.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllAreas(items: any) {
    this.finalListOfAreas = [];
  }

  getChefDetails() {
    this.adminService
      .getChefDetails(this.internalID)
      .pipe(first())
      .subscribe(
        data => {
        this.selectedAreas = data.operationalRegion;
        this.finalListOfAreas = data.operationalRegion;
        this.selectedCuisines = data.cuisineType;
        this.finalListOfCousines = data.cuisineType;
        this.selectedSpecialties = data.areaOfExpertise;
        this.finalListOfSpecialties = data.areaOfExpertise;
        this.internalID = data.chefDetails.id;
      },
      error => {
        console.log(error);
      }
    );
}

updateProfession() {
  this.isBusy = true;
  var newProfession: ProfessionUpdate = new ProfessionUpdate(
    this.finalListOfAreas,
    this.finalListOfCousines,
    this.finalListOfSpecialties,
    this.internalID
  );
  console.log('Show profession');
  console.log(newProfession);

  this.adminService.updateProfession(newProfession).subscribe(
    data => {
      this.isBusy = false;
      Swal.fire({
        title: "All done",
        text: "Profession successfully updated!",
        type: "success",
        confirmButtonColor: "#AA122B"
      }).then(result => this.router.navigate(["/view-chef/"+ this.internalID]));
    },
    error => {
      this.isBusy = false;
      Swal.fire({
        title: "Error",
        text: error.error.error,
        type: "error",
        confirmButtonColor: "#AA122B"
      });
    }
  );
}

}
