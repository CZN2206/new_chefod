import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Chef } from "src/app/models/chef";
import { AdminService } from "src/app/services/admin.service";
import { first } from "rxjs/operators";
import { ChefDataService } from "src/app/services/chef-data.service";

@Component({
  selector: "app-all-chefs",
  templateUrl: "./all-chefs.component.html",
  styleUrls: ["./all-chefs.component.css"]
})
export class AllChefsComponent implements OnInit {
  allChefs: any = [];
  selectedStatus: string = "Filter by status";
  loading: boolean;
  constructor(
    private router: Router,
    private adminService: AdminService,
    private chefData: ChefDataService
  ) {}

  ngOnInit() {
    this.loading = false;
    window.scrollTo(0, 0);
    this.getChefs("All");
  }

  changeStatus(item: any) {
    this.selectedStatus = item;
    this.getChefs(this.selectedStatus);
  }

  getChefs(status: string) {
    this.loading = true;
    this.adminService
      .getAllChefs(status)
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.allChefs = data;
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
  }

  addChef() {
    this.router.navigate(["/create-chefs"]);
  }

  viewChefDetails(internalId: string) {
    console.log('internal id = '+ internalId);
    sessionStorage.setItem("internalId", internalId);
    this.chefData.setChef(internalId);
    this.router.navigate(["/view-chef/" + internalId]);
  }
}
