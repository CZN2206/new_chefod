import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChefDataService {
  private chefData = new BehaviorSubject<string>(null);

  constructor() { }

  setChef(chefData: string){
    this.chefData.next(chefData);
  }

  getChef(){
    return this.chefData.asObservable();
  }
}
