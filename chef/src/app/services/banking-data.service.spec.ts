import { TestBed } from '@angular/core/testing';

import { BankingDataService } from './banking-data.service';

describe('BankingDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BankingDataService = TestBed.get(BankingDataService);
    expect(service).toBeTruthy();
  });
});
