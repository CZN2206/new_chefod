import { TestBed } from '@angular/core/testing';

import { ChefDataService } from './chef-data.service';

describe('ChefDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChefDataService = TestBed.get(ChefDataService);
    expect(service).toBeTruthy();
  });
});
