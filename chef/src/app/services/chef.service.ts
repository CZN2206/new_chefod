import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Dish } from "../models/dish";
import { Address } from "../models/address";
import { Banking } from "../models/banking";
import { PersonalUpdate } from '../models/personalUpdate';
import { Documents } from '../models/documentUpdate';
import { ProfessionUpdate } from '../models/professionUpdate';
import { Availability } from '../models/availability';
const endpoint = environment.baseUrl;

@Injectable({
  providedIn: "root"
})
export class ChefService {
  user: any = JSON.parse(sessionStorage.getItem("User"));
  constructor(private http: HttpClient) {}

  confirmAccount(password: string, token: string) {
    return this.http.post<any>(endpoint + "api/auth/register/chef", {
      password,
      token
    });
  }

  createDish(dish: Dish) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "create/dish", dish, {
      headers: headers
    });
  }

  getDishes() {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.get<any>(endpoint + "get/dishes", { headers: headers });
  }

  updateAddress(address: Address) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "update/address", address, {
      headers: headers
    });
  }

  updateBanking(banking: Banking) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "update/banking", banking, {
      headers: headers
    });
  }

  updatePersonal(personalUpdate: PersonalUpdate) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "update/personal/details", personalUpdate, {headers: headers});
  }

  updateProfession(profession: ProfessionUpdate){
    let headers = new HttpHeaders();
    headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);
    return this.http.post<any>(endpoint + 'update/professional/details', profession, { headers: headers})   
   }

  updateDocument(documents: Documents) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "update/documents", documents, {
      headers: headers
    });
  }

  getChefProfile() {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.get<any>(endpoint + "get/chef/profile/local", {
      headers: headers
    });
  }

  getChefProfileAdmin(internalId: string) {
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(
      endpoint + "get/chef/profile",
      { internalId },
      { headers: headers }
    );
  }

  setAvilability(availability: Availability){
    debugger;
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(
      endpoint + "create/nonavailable/date",
       availability ,
      { headers: headers }
    ); 
  }

  getAvailability(){
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.get<any>(endpoint + "get/all/nonavailable/days", {
      headers: headers
    });  
  }

  deleteAvailability(internalId: string){
    let headers = new HttpHeaders();
    headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
    return this.http.post<any>(endpoint + "delete/nonavailable/date", { internalId }, {
      headers: headers
    });   
  }
  
  
}
