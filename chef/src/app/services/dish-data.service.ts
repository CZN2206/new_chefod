import { Injectable } from '@angular/core';
import { Dish } from '../models/dish';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DishDataService {
  private dishData = new BehaviorSubject<Dish>(null);
  constructor() { }

  setDish(dish: Dish){
    this.dishData.next(dish);
  }

  getDish(){
    return this.dishData.asObservable();
  }
}
