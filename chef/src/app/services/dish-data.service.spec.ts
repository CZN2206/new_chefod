import { TestBed } from '@angular/core/testing';

import { DishDataService } from './dish-data.service';

describe('DishDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DishDataService = TestBed.get(DishDataService);
    expect(service).toBeTruthy();
  });
});
