import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Banking } from '../models/banking';

@Injectable({
  providedIn: 'root'
})
export class BankingDataService {
  private bankingData = new BehaviorSubject<Banking>(null);

  constructor() { }

  setBanking(bankingData: Banking){
    console.log('setting banking');
    console.log(bankingData);
    this.bankingData.next(bankingData);
  }

  getBanking(){
    console.log('get banking');
    console.log(this.bankingData.value);
    return this.bankingData.asObservable();
  }
}
