import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthDataService {

  constructor() { }
  private subject = new Subject<number>();


  userPermissions: Array<number> = [];
  user: any;
  public setUserPermissions(userType: number) {
    console.log('inside set perm');
    if (Number.isNaN(userType)) {
      console.log('*')
        this.subject.next(userType);
    } else {
      console.log('***')
        this.userPermissions = [];
        this.userPermissions.push(userType);
        console.log(this.userPermissions)
        this.subject.next(userType);
    }
  }

  public getPermissions(): Array<number> {
    this.user = parseInt(sessionStorage.getItem('UserType'));
    if (this.user != null) {
      this.setUserPermissions(parseInt(this.user));
    } else {
      this.userPermissions = [];
    }
    return this.userPermissions;
  }

  _setStatus(userPermission:number) {
    this.subject.next(userPermission);
  }

  _clearStatus() {
    this.subject.next();
  }

  _getStatus(): Observable<any> {
    return this.subject.asObservable();
  }

}
