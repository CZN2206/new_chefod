import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Chef } from '../models/chef';
import { ChefInvite } from '../models/chefInvite';
import { Address } from '../models/address';
import { Banking } from '../models/banking';
import { PersonalUpdate } from '../models/personalUpdate';
import { ProfessionUpdate } from '../models/professionUpdate';
import { Documents } from '../models/documentUpdate';

const endpoint = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  user: any = JSON.parse(sessionStorage.getItem('User'));
  userToken: string = "";
  constructor(private http: HttpClient) { }

  login(username: string, password: string){
    return this.http.post<any>(endpoint + 'api/auth/signin', {username, password})
 }

 inviteChef(chef: ChefInvite){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);
  console.log('create chef service');
  console.log(headers);
  return this.http.post<any>(endpoint + 'invite/chef', chef,  { headers: headers})
 }


 getDishes(){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);
  return this.http.get<any>(endpoint + 'get/dishes/to/approve',  { headers: headers})
 }

 getAllChefs(status: string){
   console.log(this.user);
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);  
  return this.http.post<any>(endpoint + 'get/all/chefs/by/status', {status}, { headers: headers}) 
 }

 getChefDetails(internalId: string){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);  
  return this.http.post<any>(endpoint + 'get/chef/profile', {internalId}, { headers: headers})
 }

 approveDish(internalId: string, status: string){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);  
  return this.http.post<any>(endpoint + 'approve/dish', {internalId, status}, { headers: headers})
 }


 smsChef(mobileNumber: string, application: string, message: string){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);  
  return this.http.post<any>(endpoint + 'send/message', {mobileNumber, application, message}, { headers: headers})
 }


 emailChef(internalId: string, subject: string, message: string){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);  
  return this.http.post<any>(endpoint + 'send/email', {internalId, subject, message}, { headers: headers})
 }

 getChefProfileAdmin(internalId: string) {
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "get/chef/profile", { internalId }, { headers: headers } );
}
 
updateAddress(address: Address) {
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "update/address", address, {
    headers: headers
  });
}

updateBanking(banking: Banking) {
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "update/banking", banking, {
    headers: headers
  });
}

updatePersonal(personalUpdate: PersonalUpdate) {
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "update/personal/details", personalUpdate, {headers: headers});
}

updateProfession(profession: ProfessionUpdate){
  let headers = new HttpHeaders();
  headers =  headers.set('Authorization','Bearer '+ this.user.accessToken);
  return this.http.post<any>(endpoint + 'update/professional/details', profession, { headers: headers})   
 }

updateDocument(documents: Documents) {
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "update/documents", documents, {
    headers: headers
  });
}

approveChef(internalId: string, status: string){
  let headers = new HttpHeaders();
  headers = headers.set("Authorization", "Bearer " + this.user.accessToken);
  return this.http.post<any>(endpoint + "update/chefs/status", {internalId, status}, {
    headers: headers
  }); 
}
}
