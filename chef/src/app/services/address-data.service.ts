import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Address } from '../models/address';

@Injectable({
  providedIn: 'root'
})
export class AddressDataService {
  private addressData = new BehaviorSubject<Address>(null);
  constructor() { }

  setAddress(address: Address){
    console.log('received address to set ');
    console.log(address);
    this.addressData.next(address);
  }

  getAddress(){
    return this.addressData.asObservable();
  }
}
