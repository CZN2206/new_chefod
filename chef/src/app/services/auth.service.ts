import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthDataService } from './auth-data.service';
import { AuthGroup } from '../models/AuthGroup';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  permissions: Array<number>;
  constructor(private authorizationDataService: AuthDataService) { }

  hasPermission(authGroup: AuthGroup) {
    if (this.permissions && this.permissions.find(permission => {
      return permission === authGroup;
    })) {
      return true;
    } else {
      this.initializePermissions();
      console.log(this.permissions);
      if (this.permissions && this.permissions.find(permission => {
        return permission == authGroup;
      })) {
        return true;
      }
    }
    return false;
  }

  // This method is called once and a list of permissions is stored in the permissions property
  initializePermissions() {
    console.log('initialise guards!!!!!!!');
    return new Promise((resolve, reject) => {
      // Call API to retrieve the list of actions this user is permitted to perform. (Details not provided here.)
      // In this case, the method returns a Promise, but it could have been implemented as an Observable
      this.permissions = this.authorizationDataService.getPermissions();
        resolve();
    });
  }
}
