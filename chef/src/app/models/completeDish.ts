export class CompleteDish{
    public itemName: string;
    public picture: string;
    public description: string;
    public itemType: string;  
    public internalId: string;
    public  status: string;
    
    constructor(itemName: string, picture: string, description: string, itemType: string, internalId: string, status: string){
        this.itemName = itemName;
        this.picture = picture;
        this.description = description;
        this.itemType = itemType;
        this.internalId = internalId;
        this.status = status;
    }
    }