export class Dish{
    public itemName: string;
    public picture: string;
    public description: string;
    public itemType: string; 
    
    constructor(itemName: string, picture: string, description: string, itemType: string){
        this.itemName = itemName;
        this.picture = picture;
        this.description = description;
        this.itemType = itemType;
    }
    }