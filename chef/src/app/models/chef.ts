import { post } from 'selenium-webdriver/http';
import { CuisineType } from './cuisineType';
import { OperationalRegion } from './operationalRegion';
import { AreaOfExpertise } from './areaOfExpertise';

export class Chef{
    public avatar: string;
    public name: string;
    public surname: string;
    public email: string;  
    public idNumber: string;
    public aboutChef: string;
    public contactNumber: string;
    public addressLine1: string;
    public addressLine2: string;
    public city: string;  
    public province: string;
    public postalCode: string;
    public pantsSize: string;
    public shirtSize: string;
    public operationalRegion: string [];
    public bankName: string;  
    public accountNumber: string;
    public branchCode: string;
    public accountType: string;
    public createdBy: string; 
    public areaOfExpertise: string [];
    public cuisineType: string [];


    constructor(avatar: string, name: string, surname: string, email: string, idNumber:string, aboutChef: string, contactNumber: string, addressLine1:  string, addressLine2: string,
        city: string, province: string, postalCode: string, pantsSize: string, shirtSize: string, operationalRegion: string [], bankName: string, accountNumber: string, branchCode: string,
        accountType: string,  createdBy: string, areaOfExpertise: string [], cuisineType: string []){
        this.avatar = avatar;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.aboutChef = aboutChef;
        this.contactNumber = contactNumber;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.pantsSize = pantsSize;
        this.shirtSize = shirtSize;
        this.operationalRegion = operationalRegion;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.branchCode = branchCode;
        this.accountType = accountType;
        this.createdBy = createdBy;
        this.areaOfExpertise = areaOfExpertise;
        this.cuisineType = cuisineType;
    }
    }