export class Admin{
public fullName: string;
public email: string;
public contactNumber: string;
public password: string;  

constructor(fullName: string, email: string, contactNumber: string, password: string){
    this.fullName = fullName;
    this.email = email;
    this.contactNumber = contactNumber;
    this.password = password;
}
}