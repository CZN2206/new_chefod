
export class PersonalUpdate{
    public internalId: string;
    public avatar: string;
    public name: string;
    public surname: string;
    public email: string; 
    public contactNumber: string;
    public idNumber: string;
    public aboutChef: string;
    public sacaNumber: string;
    public pantsSize: string;
    public shirtSize: string;

    constructor(internalId: string,avatar: string, name: string, surname: string, email: string, contactNumber: string, 
        idNumber: string, aboutChef: string, sacaNumber: string, pantsSize: string, shirtSize: string){
        this.internalId = internalId;
        this.avatar = avatar;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.contactNumber = contactNumber;
        this.idNumber = idNumber;
        this.aboutChef = aboutChef;
        this.sacaNumber = sacaNumber;
        this.pantsSize = pantsSize;
        this.shirtSize = shirtSize;
    }
    }