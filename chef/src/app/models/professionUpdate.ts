export class ProfessionUpdate{
    public operationalRegion: string []; 
    public cuisineType: string [];
    public areaOfExpertise: string [];
    public internalId: string;
  
    constructor(operationalRegion: string[], cuisineType: string [],  areaOfExpertise: string[], internalId: string){
        this.operationalRegion = operationalRegion;
        this.cuisineType = cuisineType;
        this.areaOfExpertise = areaOfExpertise;
        this.internalId = internalId;
    }
  
  }