export class Availability{
    public date: string; 
    public comment: string;
  
    constructor(date: string, comment: string){
        this.date = date;
        this.comment = comment;
    }
  
  }