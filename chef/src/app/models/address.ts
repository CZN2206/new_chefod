
export class Address{
    public addressLine1: string;
    public addressLine2: string;
    public city: string;
    public province: string;
    public postalCode: string; 
    public internalId: string;  
    
    constructor(addressLine1: string, addressLine2: string, city: string, province: string, postalCode: string, internalID: string){
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.internalId = internalID;
    }
    }