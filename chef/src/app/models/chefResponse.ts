import { post } from 'selenium-webdriver/http';
import { CuisineType } from './cuisineType';
import { OperationalRegion } from './operationalRegion';
import { AreaOfExpertise } from './areaOfExpertise';

export class ChefResponse{
    public avatar: string;
    public name: string;
    public surname: string;
    public email: string;  
    public idNumber: string;
    public contactNumber: string;
    public status: string;


    constructor(avatar: string, name: string, surname: string, email: string, idNumber:string, 
                contactNumber: string, status: string){
        this.avatar = avatar;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.contactNumber = contactNumber;
        this.status = status;
    }
    }