import { post } from 'selenium-webdriver/http';

export class Banking{
    public bankName: string;
    public accountNumber: string;
    public branchCode: string;
    public accountType: string;
    public internalId: string; 
    
    constructor(bankName: string, accountNumber: string, branchCode: string, accountType: string, internalId: string){
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.branchCode = branchCode;
        this.accountType = accountType;
        this.internalId = internalId;
    }
    }