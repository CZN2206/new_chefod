export class Documents{
    public idDocument: string;
    public policeClearance: string;
    public referrals: string;
    public qualifications: string; 
    public internalId: string; 
    
    constructor(idDocument: string, policeClearance: string, referrals: string, qualifications: string, internalId: string){
        this.idDocument = idDocument;
        this.policeClearance = policeClearance;
        this.referrals = referrals;
        this.qualifications = qualifications;
        this.internalId = internalId;
    }
    }