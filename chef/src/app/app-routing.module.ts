import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { AdminHomeComponent } from "./admin/admin-home/admin-home.component";
import { ManageChefsComponent } from "./admin/manage-chefs/manage-chefs.component";
import { ContactChefsComponent } from "./admin/contact-chefs/contact-chefs.component";
import { AllChefsComponent } from "./admin/all-chefs/all-chefs.component";
import { ViewChefComponent } from "./admin/view-chef/view-chef.component";
import { ChefHomeComponent } from "./chef/chef-home/chef-home.component";
import { ManageDishesComponent } from "./chef/manage-dishes/manage-dishes.component";
import { ConfirmAccountComponent } from "./chef/confirm-account/confirm-account.component";
import { ApproveDishesComponent } from "./admin/approve-dishes/approve-dishes.component";
import { AccountErrorComponent } from "./chef/account-error/account-error.component";
import { AddressComponent } from "./chef/profile-update/address/address.component";
import { AddressComponentAdmin } from "./admin/profile-update/address/address.component";
import { BankingComponent } from "./chef/profile-update/banking/banking.component";
import { PersonalComponent } from "./chef/profile-update/personal/personal.component";
import { ProfessionComponent } from "./chef/profile-update/profession/profession.component";
import { ViewProfileComponent } from "./chef/view-profile/view-profile.component";
import { DocumentsComponent } from './chef/profile-update/documents/documents.component';
import { BankingComponentAdmin } from './admin/profile-update/banking/banking.component';
import { PersonalComponentAdmin } from './admin/profile-update/personal/personal.component';
import { ProfessionComponentAdmin } from './admin/profile-update/profession/profession.component';
import { DocumentsComponentAdmin } from './admin/profile-update/documents/documents.component';
import { AvailabilityComponent } from './chef/availability/availability.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "admin-home",
    component: AdminHomeComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "create-chefs",
    component: ManageChefsComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "contact-chefs",
    component: ContactChefsComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "manage-chefs",
    component: AllChefsComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "view-chef/:internalID",
    component: ViewChefComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "chef-home",
    component: ChefHomeComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "manage-dishes",
    component: ManageDishesComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "confirm-chef",
    component: ConfirmAccountComponent
  },
  {
    path: "approve-dishes",
    component: ApproveDishesComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "error",
    component: AccountErrorComponent
  },
  {
    path: "address-update",
    component: AddressComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "address-update-admin/:internalID",
    component: AddressComponentAdmin,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "banking-update",
    component: BankingComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "banking-update-admin/:internalID",
    component: BankingComponentAdmin,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "personal-update",
    component: PersonalComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "personal-update-admin/:internalID",
    component: PersonalComponentAdmin,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "profession-update",
    component: ProfessionComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "profession-update-admin/:internalID",
    component: ProfessionComponentAdmin,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "documents-update",
    component: DocumentsComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "documents-update-admin/:internalID",
    component: DocumentsComponentAdmin,
    canActivate: [AuthGuard], data: {
      auth:
        [1]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "view-profile",
    component: ViewProfileComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "chef-availability",
    component: AvailabilityComponent,
    canActivate: [AuthGuard], data: {
      auth:
        [3]
    },
    runGuardsAndResolvers: 'always'
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
