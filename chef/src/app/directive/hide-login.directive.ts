import { Directive, Input, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthDataService } from '../services/auth-data.service';

@Directive({
  selector: '[appHideLogin]'
})
export class HideLoginDirective {
  @Input('hideWhenLogin') isLoggedIn: string;
  subscription: Subscription;


  constructor(private el: ElementRef, private authorizationService: AuthDataService) {

    this.subscription = this.authorizationService._getStatus().subscribe(status => {
      if (Number.isNaN(status)) {

      }
      else {
        if (this.isLoggedIn === "0") {
          this.el.nativeElement.style.display = 'none';
        } else {
          this.el.nativeElement.style.display = 'inline';
        }
      }
    }
    );
  }





  ngOnInit() {

    if (this.authorizationService.getPermissions().length === 0) {

      if (this.isLoggedIn === "1") {
        this.el.nativeElement.style.display = 'none';
      }
    } else {
      if (this.isLoggedIn === "0") {
        this.el.nativeElement.style.display = 'none';
      } else {
        this.el.nativeElement.style.display = 'inline';
      }
    }
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    console.log('ngOnDestroy')
    this.subscription.unsubscribe();
  }

}
