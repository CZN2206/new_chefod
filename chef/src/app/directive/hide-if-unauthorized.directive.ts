import { Directive, Input, ElementRef } from '@angular/core';
import { AuthGroup } from '../models/AuthGroup';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AuthDataService } from '../services/auth-data.service';

@Directive({
  selector: '[appHideIfUnauthorized]'
})
export class HideIfUnauthorizedDirective {
  @Input('myHideIfUnauthorized') permission: AuthGroup; // Required permission passed in
  subscription: Subscription;
  constructor(private el: ElementRef, 
    private authorizationService: AuthService, 
    private authorizationDataService: AuthDataService) {
  
  }

  ngOnInit() {
    this.subscription = this.authorizationDataService._getStatus().subscribe(status => {
      if (Number.isNaN(status)) {
      }else if(status == 1){
        this.el.nativeElement.style.display = 'inline';
      }else{
        this.el.nativeElement.style.display = 'none';
      }
    });

    console.log(this.permission)
    if (!this.authorizationService.hasPermission(this.permission)) {
      this.el.nativeElement.style.display = 'none';
    }
  }

 

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    console.log('ngOnDestroy')
    this.subscription.unsubscribe();
}

}
