import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { AdminHomeComponent } from "./admin/admin-home/admin-home.component";
import { AppRoutingModule } from "./app-routing.module";
import { RouterModule, Routes } from "@angular/router";
import { ManageChefsComponent } from "./admin/manage-chefs/manage-chefs.component";
import { NavComponent } from "./nav/nav.component";
import { FooterComponent } from "./footer/footer.component";
import { ContactChefsComponent } from "./admin/contact-chefs/contact-chefs.component";
import { AllChefsComponent } from "./admin/all-chefs/all-chefs.component";
import { ViewChefComponent } from "./admin/view-chef/view-chef.component";
import { ChefHomeComponent } from "./chef/chef-home/chef-home.component";
//import { ModalModule } from 'ngx-bootstrap/modal';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown-angular7";
import { ChefNavComponent } from "./chef/chef-nav/chef-nav.component";
import { ManageDishesComponent } from "./chef/manage-dishes/manage-dishes.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ConfirmAccountComponent } from "./chef/confirm-account/confirm-account.component";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
import { HttpClientModule } from "@angular/common/http";
import { ApproveDishesComponent } from "./admin/approve-dishes/approve-dishes.component";
import { AccountErrorComponent } from "./chef/account-error/account-error.component";
import { PersonalComponent } from "./chef/profile-update/personal/personal.component";
import { BankingComponent } from "./chef/profile-update/banking/banking.component";
import { AddressComponent } from "./chef/profile-update/address/address.component";
import {AddressComponentAdmin} from "./admin/profile-update/address/address.component"
import { ProfessionComponent } from "./chef/profile-update/profession/profession.component";
import { ViewProfileComponent } from './chef/view-profile/view-profile.component';
import { NgxDropzoneModule } from "ngx-dropzone";
import { DropzoneModule} from "ngx-dropzone-wrapper";

import { DocumentsComponent } from "./chef/profile-update/documents/documents.component";
import { NgxDocViewerModule } from "ngx-doc-viewer"
import { BankingComponentAdmin } from './admin/profile-update/banking/banking.component';
import { PersonalComponentAdmin } from './admin/profile-update/personal/personal.component';
import { ProfessionComponentAdmin } from './admin/profile-update/profession/profession.component';
import { DocumentsComponentAdmin } from './admin/profile-update/documents/documents.component';
import { AvailabilityComponent } from './chef/availability/availability.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './services/auth.service';
import { HideIfUnauthorizedDirective } from './directive/hide-if-unauthorized.directive';
import { HideLoginDirective } from './directive/hide-login.directive';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminHomeComponent,
    ManageChefsComponent,
    NavComponent,
    FooterComponent,
    ContactChefsComponent,
    AllChefsComponent,
    ViewChefComponent,
    ChefHomeComponent,
    ChefNavComponent,
    ManageDishesComponent,
    ConfirmAccountComponent,
    ApproveDishesComponent,
    AccountErrorComponent,
    PersonalComponent,
    BankingComponent,
    AddressComponent,
    AddressComponentAdmin,
    BankingComponentAdmin,
    PersonalComponentAdmin,
    ProfessionComponentAdmin,
    ProfessionComponent,
    ViewProfileComponent,
    DocumentsComponent,
    DocumentsComponentAdmin,
    AvailabilityComponent,
    HideIfUnauthorizedDirective,
    HideLoginDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    NgxDropzoneModule,
    DropzoneModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: "never" }),
    HttpClientModule,
    NgxDocViewerModule,
    SweetAlert2Module.forRoot(),
    //ModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule {}
