import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthGroup } from 'src/app/models/AuthGroup';

@Component({
  selector: "app-chef-nav",
  templateUrl: "./chef-nav.component.html",
  styleUrls: ["./chef-nav.component.css"]
})
export class ChefNavComponent implements OnInit {
  authgroup: AuthGroup = 3;
  constructor(private router: Router) {}

  ngOnInit() {
    if (window.localStorage) {
      if (!localStorage.getItem("firstLoad")) {
        console.log('APP nav init')
        localStorage["firstLoad"] = true;
        window.location.reload();
      } else localStorage.removeItem("firstLoad");
    }
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(["/login"]);
  }
}
