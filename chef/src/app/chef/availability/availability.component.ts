import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChefService } from 'src/app/services/chef.service';
import Swal from 'sweetalert2';
import { Availability } from 'src/app/models/availability';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css']
})
export class AvailabilityComponent implements OnInit {
  availabilityUpdateForm: FormGroup;
  isBusy: boolean;
  availabilityDates: any [];
  loading: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router, private chefService: ChefService,) { }

  ngOnInit() {
    this.isBusy = false;

    this.availabilityUpdateForm = this.formBuilder.group({
      availabilityDate: ["", Validators.required],
      comment: ["", Validators.required]
    });

    this.getAllAvailability();
  }

  getAllAvailability(){
    console.log('get dartes!');
    this.loading = true;
    this.chefService.getAvailability().pipe(first())
    .subscribe(
      data => {
        this.loading = false;
        this.availabilityDates = data;  
        console.log(data);     
      },
      error => {
        this.loading = false;
         console.log(error.error.errorMessage);
         this.availabilityDates = [];
      }
    ) 
  }

  get formData(){
    return this.availabilityUpdateForm.controls;
  }

  setAvailability(){
    this.isBusy = true;
    if(this.availabilityUpdateForm.invalid){
      Swal.fire({
        title: 'Warning',
        text: 'Please comlpete all fields!',
        type: 'warning',
        confirmButtonColor: '#AA122B',
    })  
      return;
    }
  
    var availability: Availability = new Availability(
      this.formData.availabilityDate.value,
      this.formData.comment.value
    )

    console.log('show availability before call');
    console.log(availability);
  
    this.chefService.setAvilability(availability).pipe(first())
  .subscribe(
    data => {
      this.isBusy = false;
      Swal.fire({
        title: 'All done',
        text: 'Availability successfully set!',
        type: 'success',
        confirmButtonColor: '#AA122B',
      }).then(result => this.getAllAvailability());
      if (window.localStorage) {
        if (!localStorage.getItem("firstLoad")) {
          localStorage["firstLoad"] = true;
          window.location.reload();
        } else localStorage.removeItem("firstLoad");
      }
    },
    error => {
       this.isBusy = false;
       Swal.fire(
        {
          title: 'Error',
          text: error.error.error,
          type: 'error',
          confirmButtonColor: '#AA122B',
         }
       )
    }
  );
  }

  deleteAvailability(internalId: string){
    this.chefService.deleteAvailability(internalId).pipe(first())
  .subscribe(
    data => {
      this.isBusy = false;
      Swal.fire({
        title: 'All done',
        text: 'Record  successfully deleted!',
        type: 'success',
        confirmButtonColor: '#AA122B'
      }).then(result => this.getAllAvailability());
     
      if (window.localStorage) {
        if (!localStorage.getItem("firstLoad")) {
          localStorage["firstLoad"] = true;
          window.location.reload();
        } else localStorage.removeItem("firstLoad");
      } 
    },
    error => {
       this.isBusy = false;
       Swal.fire(
        {
          title: 'Error',
          text: error.error.error,
          type: 'error',
          confirmButtonColor: '#AA122B',
         }
       )
    }
  ); 
   
  }

}
