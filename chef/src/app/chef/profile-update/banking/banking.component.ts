import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ChefService } from "src/app/services/chef.service";
import { Banking } from "src/app/models/banking";
import Swal from "sweetalert2";
import { BankingDataService } from "src/app/services/banking-data.service";
import { first } from "rxjs/operators";
import { Address } from "src/app/models/address";

@Component({
  selector: "app-banking",
  templateUrl: "./banking.component.html",
  styleUrls: ["./banking.component.css"]
})
export class BankingComponent implements OnInit {
  updateBankingForm: FormGroup;
  bankName: string = "";
  accountNumber: string = "";
  branchCode: string = "";
  accountType: string = "";
  currentBanking: Banking;
  isBusy: boolean;
  InternalId: string;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private chefService: ChefService,
    private bankingData: BankingDataService,
    private activated: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isBusy = false;
    this.loading = false;
    this.activated.params.subscribe(params => {
      this.InternalId = params.internalID;
    });

    this.updateBankingForm = this.formBuilder.group({
      bankName: ["", Validators.required],
      accNumber: ["", Validators.required],
      accType: ["", Validators.required],
      branchCode: ["", Validators.required]
    });
 
    this.getChefDetails();
  }

  get formData() {
    return this.updateBankingForm.controls;
  }

  getChefDetails() {
    this.loading = true;
    this.chefService
      .getChefProfile()
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.bankName = data.chefDetails.bankName;
          this.accountNumber = data.chefDetails.accountNumber;
          this.accountType = data.chefDetails.accountType;
          this.branchCode = data.chefDetails.branchCode;
          this.InternalId = data.chefDetails.id;
        },
        error => {
          console.log(error);
        }
      );
  }


  updateBanking() {
    this.isBusy = true;
    var newBanking: Banking = new Banking(
      this.formData.bankName.value,
      this.formData.accNumber.value,
      this.formData.branchCode.value,
      this.formData.accType.value,
      this.InternalId
    );

    this.chefService.updateBanking(newBanking).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Banking successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/view-profile"]));
      },
      error => {
        this.isBusy = false;
        Swal.fire({
          title: "Error",
          text: error.error.error,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );
  }
}
