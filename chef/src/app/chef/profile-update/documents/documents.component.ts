import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { ChefService } from 'src/app/services/chef.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Documents } from 'src/app/models/documentUpdate';
import Swal from 'sweetalert2';
//import { NgxDropzoneComponent } from 'ngx-dropzone/lib/ngx-dropzone/ngx-dropzone.component';
//import { NgxDropzoneLabelDirective } from 'ngx-dropzone/lib/ngx-dropzone-label.directive';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  isBusy: boolean; 
  policeClearance: string = "";
  idCopy: string = "";
  referal: string = "";
  qualification: string = "";
  internalId: string = "";

  policeClearanceFile: File;
  idCopyFile: File;
  referalFile: File;
  qualificationFile: File;

  policeClearanceName: string = "Police Clearance Upload";
  qualificationName: string = "Qualification Upload";
  referralName: string = "Referral Upload";
  idName: string = "ID Upload";

  constructor(private router: Router, private chefService: ChefService, private activated: ActivatedRoute) { }

  ngOnInit() {
    this.isBusy = false;
    this.activated.params.subscribe(params => {
      console.log(params.internalID);
      this.internalId = params.internalID;
    });
    this.getChefDetails();
  }

  getChefDetails() {
   
    this.chefService
      .getChefProfile()
      .pipe(first())
      .subscribe(
        data => {
          this.internalId = data.chefDetails.id;
        },
        error => {
          console.log(error);
        }
      );
  }

  convertFile(pdfFile: File): string {
    if(pdfFile != null){
      let reader = new FileReader();
      reader.readAsDataURL(pdfFile);
      reader.onload = (e: ProgressEvent) => {
        return reader.result.toString();
        console.log('success');
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };   
      console.log(reader.result.toString());
     // return reader.result.toString();
    }
    else{
      return "";
    }
  }

  onPoliceClearanceSelect(files: FileList) {
    this.policeClearanceName = files.item(0).name;
    this.policeClearanceFile = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(this.policeClearanceFile);
    reader.onload = (e: ProgressEvent) => {
      this.policeClearance =  reader.result.toString();
      console.log('success');
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };   
    
  }

  onReferalSelect(files: FileList) {
    this.referralName = files.item(0).name;
    this.referalFile = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(this.referalFile);
    reader.onload = (e: ProgressEvent) => {
      this.referal =  reader.result.toString();
      console.log('success');
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    }; 
  }

  onQualificationSelect(files: FileList) {
    this.qualificationName = files.item(0).name;
    this.qualificationFile = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(this.qualificationFile);
    reader.onload = (e: ProgressEvent) => {
      this.qualification =  reader.result.toString();
      console.log('success');
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    }; 
  }

  onIDSelect(files: FileList) {
    this.idName = files.item(0).name;
    this.idCopyFile = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(this.idCopyFile);
    reader.onload = (e: ProgressEvent) => {
      this.idCopy =  reader.result.toString();
      console.log('success');
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    }; 
  }

  updateDocs(){
    console.log('1 = '+ this.idCopy);
    console.log('2 = '+this.policeClearance);
    console.log('3 = '+this.referal);
    console.log('4 = '+this.qualification);

   /* console.log('1 = '+ this.idCopyFile.name);
    console.log('2 = '+ this.policeClearanceFile.name);
    console.log('3 = '+ this.referalFile.name);
    console.log('4 = '+ this.qualificationFile.name);*/

    this.isBusy = true;
    var newDocuments: Documents = new Documents(
      this.idCopy,
      this.policeClearance,
      this.referal,
      this.qualification,    
      this.internalId
    );

    console.log(newDocuments);

    this.chefService.updateDocument(newDocuments).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Documents successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/view-chef/"+ this.internalId]));
      },
      error => {
        this.isBusy = false;
        Swal.fire({
          title: "Error",
          text: error.error.error,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );
  }
}
