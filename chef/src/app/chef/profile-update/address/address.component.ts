import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ChefService } from "src/app/services/chef.service";
import { Address } from "src/app/models/address";
import Swal from "sweetalert2";
import { AddressDataService } from "src/app/services/address-data.service";
import { Chef } from "src/app/models/chef";
import { Banking } from "src/app/models/banking";
import { first } from "rxjs/operators";

@Component({
  selector: "app-address",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.css"]
})
export class AddressComponent implements OnInit {
  addressUpdateForm: FormGroup;
  line1: string = "";
  line2: string = "";
  city: string = "";
  province: string = "";
  postalCode: string = "";
  currentAddress: Address;
  isBusy: boolean;
  InternalId: string;
  userType: string;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private chefService: ChefService,
    private addressData: AddressDataService,
    private activated: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isBusy = false;
    this.loading = false;
    this.userType = sessionStorage.getItem("UserType");
    console.log("user type is " + this.userType);
    this.activated.params.subscribe(params => {
      this.InternalId = params.internalID;
    });

    this.getChefProfile();
    this.addressUpdateForm = this.formBuilder.group({
      line1: ["", Validators.required],
      line2: ["", Validators.required],
      city: ["", Validators.required],
      province: ["", Validators.required],
      postalCode: ["", Validators.required]
    });
  }

  getChefProfile() {
    console.log("go get entire chef profile");
    this.loading = true;
    this.chefService
      .getChefProfile()
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.line1 = data.chefDetails.addressLine1;
          this.line2 = data.chefDetails.addressLine2;
          this.city = data.chefDetails.city;
          this.province = data.chefDetails.province;
          this.postalCode = data.chefDetails.postalCode;
          this.InternalId = data.chefDetails.id;
        },
        error => {
          console.log(error);
        }
      );
  }

  get formData() {
    return this.addressUpdateForm.controls;
  }

  updateAddress() {
    this.isBusy = true;
    var newAddress: Address = new Address(
      this.formData.line1.value,
      this.formData.line2.value,
      this.formData.city.value,
      this.formData.province.value,
      this.formData.postalCode.value,
      this.InternalId
    );

    console.log(newAddress);

    this.chefService.updateAddress(newAddress).subscribe(
      data => {
        this.isBusy = false;
        Swal.fire({
          title: "All done",
          text: "Address successfully updated!",
          type: "success",
          confirmButtonColor: "#AA122B"
        }).then(result => this.router.navigate(["/view-profile"]));
      },
      error => {
        this.isBusy = false;
        Swal.fire({
          title: "Error",
          text: error.error.error,
          type: "error",
          confirmButtonColor: "#AA122B"
        });
      }
    );
  }
}
