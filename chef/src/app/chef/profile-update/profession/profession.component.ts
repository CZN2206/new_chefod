import { Component, OnInit } from '@angular/core';
import { AreaOfExpertise } from 'src/app/models/areaOfExpertise';
import { CuisineType } from 'src/app/models/cuisineType';
import { OperationalRegion } from 'src/app/models/operationalRegion';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfessionUpdate } from 'src/app/models/professionUpdate';
import Swal from 'sweetalert2';
import { ChefService } from 'src/app/services/chef.service';

@Component({
  selector: 'app-profession',
  templateUrl: './profession.component.html',
  styleUrls: ['./profession.component.css']
})
export class ProfessionComponent implements OnInit {
  specialtyList = [];
  selectedSpecialties: string [] = [];
  cuisineList = [];
  selectedCuisines: string [] = [];
  areaList = [];
  selectedAreas: string [] = [];
  finalListOfSpecialties: string[] = [];
  finalListOfAreas: string[] = [];
  finalListOfCousines: string[] = [];
  dropdownSettings = {};  
  professionUpdateForm: FormGroup;
  isBusy: boolean;
  InternalId: string;

  dropdownList : string [] = [];
  constructor(private formBuilder: FormBuilder, private router: Router, private chefService: ChefService,) { }

  ngOnInit() {
    this.specialtyList = [
      { item_id: 1, item_text: 'Hot Kitchen' },
      { item_id: 2, item_text: 'Baking' },
      { item_id: 3, item_text: 'Cold Kitchen' },
      { item_id: 4, item_text: 'All rounder' },
      { item_id: 5, item_text: 'Pastry' },
      { item_id: 6, item_text: 'Saucer' }
    ];

    this.cuisineList = [
      { item_id: 1, item_text: 'Italian' },
      { item_id: 2, item_text: 'Spanish' },
      { item_id: 3, item_text: 'French' },
      { item_id: 4, item_text: 'Greek' },
      { item_id: 5, item_text: 'Indian' },
      { item_id: 6, item_text: 'Chinese' },
      { item_id: 7, item_text: 'BBQ/Braai' },
      { item_id: 8, item_text: 'Mexican' },
      { item_id: 8, item_text: 'South African Cuisine' }
    ];

    this.areaList = [
      { item_id: 1, item_text: "Pretoria" },
      { item_id: 2, item_text: "Johannesburg" },
      { item_id: 3, item_text: "Cape Town" },
      { item_id: 4, item_text: "Sandton" },
      { item_id: 5, item_text: "Durban" },
      { item_id: 6, item_text: "Port Elizabeth" }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

  this.getChefDetails(); 
 
  }

  onSpecialtySelect(item: any) {
    var areaOfExpertise: AreaOfExpertise = new AreaOfExpertise(item.item_text);

    item.item_text;
    this.finalListOfSpecialties.push(item.item_text);
  }

  onSelectAllSpecialties(items: any) {
    for (var i = 0; i < items.length; i++) {
      var areaOfExpertise: AreaOfExpertise = new AreaOfExpertise(
        items[i].item_text
      );

      this.finalListOfSpecialties.push(items[i].item_text);
    }
  }

  OnSpecialtyDeSelect(item: any) {
    this.finalListOfSpecialties.splice(
      this.finalListOfSpecialties.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllSpecialties(items: any) {
    this.finalListOfSpecialties = [];
  }

  onCuisineSelect(item: any) {
    this.finalListOfCousines.push(item.item_text);
  }
  onSelectAllCuisines(items: any) {
    for (var i = 0; i < items.length; i++) {
      var cuisine: CuisineType = new CuisineType(items[i].item_text);
      this.finalListOfCousines.push(items[i].item_text);
    }
  }

  OnCuisineDeSelect(item: any) {
    this.finalListOfCousines.splice(
      this.finalListOfCousines.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllCuisines(items: any) {
    this.finalListOfCousines = [];
  }

  onAreaSelect(item: any) {
    this.finalListOfAreas.push(item.item_text);
  }

  onSelectAllAreas(items: any) {
    for (var i = 0; i < items.length; i++) {
      var area: OperationalRegion = new OperationalRegion(items[i].item_text);
      this.finalListOfAreas.push(items[i].item_text);
    }
  }

  OnAreasDeSelect(item: any) {
    this.finalListOfAreas.splice(
      this.finalListOfAreas.indexOf(item.item_text),
      1
    );
  }

  onDeSelectAllAreas(items: any) {
    this.finalListOfAreas = [];
  }

getChefDetails() {
  this.chefService
    .getChefProfile()
    .pipe(first())
    .subscribe(
      data => {
        console.log(data.operationalRegion);

        this.selectedAreas = data.operationalRegion;
        this.finalListOfAreas = data.operationalRegion;
        this.selectedCuisines = data.cuisineType;
        this.finalListOfCousines = data.cuisineType;
        this.selectedSpecialties = data.areaOfExpertise;
        this.finalListOfSpecialties = data.areaOfExpertise;
        this.InternalId = data.chefDetails.id;
      },
      error => {
        console.log(error);
      }
    );
}

updateProfession() {
  this.isBusy = true;
  var newProfession: ProfessionUpdate = new ProfessionUpdate(
    this.finalListOfAreas,
    this.finalListOfCousines,
    this.finalListOfSpecialties,
    this.InternalId
  );
  console.log('Show profession');
  console.log(newProfession);

  this.chefService.updateProfession(newProfession).subscribe(
    data => {
      this.isBusy = false;
      Swal.fire({
        title: "All done",
        text: "Profession successfully updated!",
        type: "success",
        confirmButtonColor: "#AA122B"
      }).then(result => this.router.navigate(["/view-profile"]));
    },
    error => {
      this.isBusy = false;
      Swal.fire({
        title: "Error",
        text: error.error.error,
        type: "error",
        confirmButtonColor: "#AA122B"
      });
    }
  );
}

}
