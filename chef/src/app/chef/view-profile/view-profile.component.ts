import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ChefDataService } from "src/app/services/chef-data.service";
import { AdminService } from "src/app/services/admin.service";
import { first } from "rxjs/operators";
import { Chef } from "src/app/models/chef";
import Swal from "sweetalert2";
import { ChefService } from "src/app/services/chef.service";
import { DomSanitizer } from '@angular/platform-browser';
//import { NgxDropzoneComponent } from 'ngx-dropzone/lib/ngx-dropzone/ngx-dropzone.component';
//import { NgxDropzoneLabelDirective } from 'ngx-dropzone/lib/ngx-dropzone-label.directive';

@Component({
  selector: "app-view-profile",
  templateUrl: "./view-profile.component.html",
  styleUrls: ["./view-profile.component.css"]
})
export class ViewProfileComponent implements OnInit {
  //@ViewChild(NgxDropzoneComponent, { static: false }) componentRef?: NgxDropzoneComponent;
  //@ViewChild(NgxDropzoneLabelDirective, { static: false }) directiveRef?: NgxDropzoneLabelDirective;
  dropzone: any;
  chefId: string;
  chefDetails: any;
  InternalId: any;
  idCopy: File;
  url: any;

  idAvailable: boolean;
  pcAvailable: boolean;
  qualAvailable: boolean;
  refAvailable: boolean;

  constructor(
    private router: Router,
    private chefData: ChefDataService,
    private chefService: ChefService,
    private activated: ActivatedRoute,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    window.scrollTo(0, 0);
    this.qualAvailable = false;
    this.pcAvailable = false;
    this.refAvailable = false;
    this.idAvailable = false;

    this.chefData.getChef().subscribe(chef => {
      this.chefId = chef;
      //this.getChefDetails(chef)
    });

    this.activated.params.subscribe(params => {
      this.InternalId = params.internalID;
    });

    console.log(this.InternalId);

    this.getChefDetails();
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(
      `linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`
    );
  }


  getChefDetails() {
    this.chefService
      .getChefProfile()
      .pipe(first())
      .subscribe(
        data => {
          this.chefDetails = data;
          this.idCopy = data.chefDetails.id;
          if(this.chefDetails.chefDetails.idDocument != ""){
            this.idAvailable = true;
          }
          if(this.chefDetails.chefDetails.policeClearance != ""){
            this.pcAvailable = true;
          }
          if(this.chefDetails.chefDetails.qualifications != ""){
            this.qualAvailable = true;
          }
          if(this.chefDetails.chefDetails.referrals != ""){
            this.refAvailable = true;
          }
          console.log(this.refAvailable );
          console.log(this.qualAvailable );
          console.log(this.pcAvailable );
          console.log(this.idAvailable );
          sessionStorage.setItem("ChefContact", data.chefDetails.contactNumber);
        },
        error => {
          console.log(error);
        }
      );
  }

  readUrl(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
  
      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
      }
  
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  previewDocs(){
    //const dropzone = this.componentRef.directiveRef.dropzone();
    const mockFile = { name: "idCopy", size: "5kb" };

    this.dropzone.emit( "addedfile", mockFile );
    //dropzone.emit( "thumbnail", mockFile, res.profileImageUrl );
    this.dropzone.emit( "complete", mockFile);
  }

  contactChef() {
    this.router.navigate(["/contact-chefs"]);
  }

  updateAddress() {
    this.router.navigate(["/address-update"]);
  }

  updatePersonal() {
    console.log('clicked personal');
    this.router.navigate(["/personal-update"]);
  }

  updateBanking() {
    this.router.navigate(["/banking-update"]);
  }

  updateDocs() {
    this.router.navigate(["/documents-update"]);
  }

  updateProfession() {
    this.router.navigate(["/profession-update/"]);
  }
  
  
  downloadPDF(name: string){
    var linkSource;
    if(name == 'idDocument'){
       linkSource = this.chefDetails.chefDetails.idDocument;
    }
    if(name == 'referral'){
       linkSource = this.chefDetails.chefDetails.referrals;
    }
    if(name == 'qualification'){
       linkSource = this.chefDetails.chefDetails.qualifications;
    }
    if(name == 'police'){
       linkSource = this.chefDetails.chefDetails.policeClearance;
    }
  
    const downloadLink = document.createElement("a");
    const fileName = name+'.pdf';
  
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}
