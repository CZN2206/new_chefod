import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { ChefService } from "src/app/services/chef.service";
import { first } from "rxjs/operators";
import { Banking } from "src/app/models/banking";
import { Address } from "src/app/models/address";
import { ChefDataService } from "src/app/services/chef-data.service";
import { AddressDataService } from "src/app/services/address-data.service";
import { BankingDataService } from "src/app/services/banking-data.service";

@Component({
  selector: "app-chef-home",
  templateUrl: "./chef-home.component.html",
  styleUrls: ["./chef-home.component.css"]
})
export class ChefHomeComponent implements OnInit {
  constructor(
    private chefService: ChefService,
    private chefData: ChefDataService,
    private addressData: AddressDataService,
    private bankingData: BankingDataService
  ) {}

  modalData: {
    dishType: string;
    dishName: string;
    dishImage: string;
  };

  ngOnInit() {

  }


}
