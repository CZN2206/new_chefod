import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-error',
  templateUrl: './account-error.component.html',
  styleUrls: ['./account-error.component.css']
})
export class AccountErrorComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.router.navigate(['/confirm-chef']);
  }
}
