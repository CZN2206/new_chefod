import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountErrorComponent } from './account-error.component';

describe('AccountErrorComponent', () => {
  let component: AccountErrorComponent;
  let fixture: ComponentFixture<AccountErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
