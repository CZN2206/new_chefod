import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChefService } from 'src/app/services/chef.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent implements OnInit {

  confirmAccountForm: FormGroup;
  magicToken: string = '';
  password: string;
  isBusy: boolean;
  showSpinner: string = '1';
  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private chefService: ChefService, private router: Router) {
  }

  ngOnInit() {
    console.log(this.isBusy);
   this.confirmAccountForm = this.formBuilder.group(
      {password: ['', Validators.required],
       confirmPassword:['',Validators.required
    ]}
    )    

    this.activatedRoute.queryParams.subscribe(params => {
      this.magicToken = params.token;
  });
  this.isBusy = false;
  }

  checkPasswords() { // here we have the 'passwords' group
    let pass = this.formData.password.value;  
    let confirmPass = this.formData.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }     
  }

  get formData(){
    return this.confirmAccountForm.controls;
  }


  onSubmit(){    
    if(this.confirmAccountForm.invalid){
      Swal.fire(
        'Warning',
        'Passwords must be atleast 6  characters long!',
        'info'
      )
      return;
    }
    this.isBusy = true;

    if(this.formData.password.value !== this.formData.confirmPassword.value){
        this.isBusy = false;
        Swal.fire(
          'Error',
          'Passwords dont match!',
          'error'
        )
        return;
    }

  this.chefService.confirmAccount(this.confirmAccountForm.controls.password.value, this.magicToken).subscribe(
    data => {
      this.isBusy = false;
      Swal.fire(
        {
          title: 'All done',
          text: 'Account confirmed successfully!',
          type: 'success',
          confirmButtonColor: '#AA122B',
         }

      ).then(
        result => this.router.navigate(['/login'])  
      );    
    },
    error => {
      this.isBusy = false;      
        Swal.fire(
          {
            title: 'Error',
            text: error.error.message,
            type: 'error',
            confirmButtonColor: '#AA122B',
           }
        ).then(
          result => this.router.navigate(['/error'])  
        )
        return;
     
      this.router.navigate(['/error']);
    }
  );
}

goHome(){
  this.router.navigate(['']);
}
}
