import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChefService } from 'src/app/services/chef.service';
import { first } from 'rxjs/operators';
import { Dish } from 'src/app/models/dish';
import { CompleteDish } from 'src/app/models/completeDish';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-manage-dishes',
  templateUrl: './manage-dishes.component.html',
  styleUrls: ['./manage-dishes.component.css']
})
export class ManageDishesComponent implements OnInit {
  title;
  isBusy: boolean;
  image: string = '';
  createDishForm: FormGroup;
  selectedItem: string = 'Category';
  selectedFile = null;
  errorMessage: string = '';
  allDishes: CompleteDish [] = [];
  loading: boolean;
  constructor(private formBuilder: FormBuilder, private _sanitizer: DomSanitizer, private router: Router, private chefService: ChefService) { }


  ngOnInit() {
    this.loading = false;
    this.isBusy = false;
    console.log('go get all dishes for chef');
    this.getAllDishes();
    this.createDishForm = this.formBuilder.group(
      {dishName: ['', Validators.required],
      dishDescription: ['', Validators.required]
    }); 
  }

  onFileChanged($event): void {
    console.log('image changes!!!!!!!!!!!');
    this.readThis($event.target);
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }

  get formData(){
    return this.createDishForm.controls;
  }


  changeCategory(item: any){
    this.selectedItem = item;
  }
  
  readThis(inputValue: any): void {
    this.selectedFile =
    inputValue.files[0];
    var myReader: FileReader =
    new FileReader();
     myReader.onloadend = (e) => {
    this.image = (myReader.result as string);
   }
   myReader.readAsDataURL(
    this.selectedFile);
}

goHome(){
  this.router.navigate(['/admin-home']); 
}

getAllDishes(){
  this.loading = true;
  this.chefService.getDishes().pipe(first())
  .subscribe(
    data => {
      this.loading = false;
      this.allDishes = data;       
    },
    error => {
      this.loading = false;
       this.errorMessage = error.error.errorMessage;
       this.allDishes = [];
    }
  ) 
}

createDish(){

  this.isBusy = true;
  if(this.createDishForm.invalid){
    Swal.fire({
      title: 'Warning',
      text: 'Please comlpete all fields!',
      type: 'warning',
      confirmButtonColor: '#AA122B',
  })  
    return;
  }

  var dish: Dish = new Dish(
    this.formData.dishName.value,
    this.image,
    this.formData.dishDescription.value,
    this.selectedItem
  )

  console.log(dish);

  this.chefService.createDish(dish).pipe(first())
  .subscribe(
    data => {
      this.isBusy = false;
      Swal.fire({
        title: 'All done',
        text: 'Dish successfully created!',
        type: 'success',
        confirmButtonColor: '#AA122B',
      }

      ).then(
        result => this.getAllDishes()); 
    
    },
    error => {
      console.log(error);
       this.errorMessage = error.error.error;
       this.isBusy = false;
       Swal.fire(
        {
          title: 'Error',
          text: this.errorMessage,
          type: 'error',
          confirmButtonColor: '#AA122B',
         }
       )
    }
  );
  this.formData.dishDescription.reset();
  this.formData.dishName.reset();
  this.selectedItem = 'Category';
  this.image = '';
  console.log('calll get dishes again')
  this.getAllDishes();

}


}
